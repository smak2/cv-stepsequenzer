# Controllertyp
MCU = atmega2560

# Quellcode-Datei mit main() Funktion
TARGET = main
SOURCES = ./src/$(TARGET).c  ./src/uart/uart.c  # Weiter durch Komma separiert. Alternativ mit '+=' Operator. Bsp: SRC += blablub.c

# Programmer einbinden
PROGRAMMER =wiring 
PORT = /dev/cu.usbmodem14101 
BAUD=115200 

#Ab hier nichts verändern
OBJECTS=$(SOURCES:.c=.o)
CFLAGS=-c -Os
LDFLAGS=

all: hex 
#all: hex eeprom

hex: $(TARGET).hex

#eeprom: $(TARGET)_eeprom.hex

$(TARGET).hex: $(TARGET).elf
	avr-objcopy -O ihex -j .data -j .text $(TARGET).elf $(TARGET).hex

#$(TARGET)_eeprom.hex: $(TARGET).elf
#	avr-objcopy -O ihex -j .eeprom --change-section-lma .eeprom=1 $(TARGET).elf $(TARGET)_eeprom.hex

$(TARGET).elf: $(OBJECTS)
	avr-gcc $(LDFLAGS) -mmcu=$(MCU) $(OBJECTS) -o $(TARGET).elf

.c.o:
	avr-gcc $(CFLAGS) -mmcu=$(MCU) $< -o ./$@

size:
	avr-size --mcu=$(MCU) -C $(TARGET).elf

program:
	avrdude -v -p$(MCU) -P$(PORT) -B$(BAUD) -c$(PROGRAMMER)  -D -Uflash:w:$(TARGET).hex:i

clean_tmp:
	rm -vrf *.o
	rm -vrf *.elf

clean:
	rm -vrf *.o
	rm -vrf *.elf
	rm -vrf *.hex

# auskommentiert: EERPOM-Inhalt wird nicht mitgeschrieben
# WRITE_EEPROM = -U eeprom:w:$(TARGET).eep
