#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#define F_CPU 16000000UL	// Taktfrequenz Prozessor
#include <util/delay.h>
#include "uart/uart.h"
#include "OnLCDlib.h"
#include "voltokt.h"

////////////////////////////////////////////////////////////////////////////////

// UI bit masks
#define PLAY_RESTART 0x1
#define STOP_PAUSE 0x2
#define LEFT 0x4
#define RIGHT 0x8
#define FREE 0x10
#define ENTER 0x20
#define UP 0x40
#define DOWN 0x80

// Konstante Werte definieren
#define NOTE_ON 128
#define NOTE_ON_DEFAULT 0 
#define BPM_MIN 10
#define BPM_MAX 300
#define NUM_TRACKS 2
#define STEP_METER 16
#define NUM_BARS 2
#define PPQ 24
#define TIME_SIG 4  //  4/4 Takt
#define PATTERN_LEN ( NUM_BARS * STEP_METER ) 
#define TICK_CNT_MAX (STEP_METER * NUM_BARS)

// Output-Shiftregister HW-PIN Konfiguration
#define D_IN_595 0
#define SH_CLK_595 1
#define ST_CLK_595 2

// Input-Shiftregister
#define SHIFT_DDR_165 DDRA
#define SHIFT_PORT_165 PORTA
#define SHIFT_PIN_165 PINA

#define CLK_INHIBIT_165 3
#define CLK_165 4
#define SH_LD_CLK_165 5
#define S_READ_165 6

// DAC PIN Konfig
#define DDR_SPI DDRB
#define DAC_SCK PB1
#define DAC_SDI_MOSI PB2
#define DAC_LDAC PB6

#define DAC_CS_GATE PB5			
#define DAC_CS_CV PB0			// SS on AVR
#define DAC_CS_CV2 PL0			// SS on AVR
#define DAC_CS_CV3 PL1			// SS on AVR

// DAC config cmd bitbanged
#define DAC_CH_A 15 
#define DAC_BUF_OUT 14
#define DAC_XGAIN 13 
#define DAC_POWER_DOWN 12 

#define TRIG_VOLTAGE 0b0000111111111111

// Global Variablen / Daten ////////////////////////////////////////////////////
//

// Sequenzer Clock
volatile uint16_t tick_cnt =0;
volatile uint8_t  mod6_cnt =0;

uint16_t bpm = 120;
uint8_t playing =1;
uint8_t step_pos = 0;				// Sequencer Step
uint8_t note_pos = 0;				// Sequencer Step
volatile uint8_t at_bar = 0;

// UI Steuerung 
uint8_t __step =0;
uint8_t selected_bar =1;
uint16_t selected_steps_release = 0;
uint16_t selected_steps_onpress = 0;

// state-machine
enum transport_state {normal,rec} machine_state;
enum select_state { bar, step, note, cv1, cv2, cv3, nlen, timingticks, tempo } select_state;
enum ctrl_mode { select, valchange} mode;
void *parameter_sel;

// Taster
volatile uint16_t seq_button_scan=0;
volatile uint16_t last_seq_button_scan = 0xffff;
volatile uint8_t ctrl_button_scan=0;
volatile uint8_t last_ctrl_button_scan = 0xff;
volatile uint8_t ctrl;

// Encoder
volatile int8_t enc_delta;          // -128 ... 127
static int8_t last;

// Seq. Datenstruktur
typedef struct stepevent_data {
	uint8_t tickstamp; 	
	uint8_t note_evnt; // >=128  Note an / aus | <=127 Noten Wert
	uint8_t note_len;
	uint16_t cv1_val;
	uint16_t cv2_val;
	uint16_t cv3_val;
} stepevent_data;

struct stepevent_data seq_pattern[PATTERN_LEN];
//typedef struct seq_pattern {
//	struct stepevent_data seq_steps[PATTERN_LEN];
//	uint8_t root_note;
//	uint16_t seq_cv1_val;
//	uint16_t seq_cv2_val;
//	uint16_t seq_cv3_val;
//}

// Digital / Analog Ansteuerung
uint16_t gate_trig_low =0;
uint16_t gate_trig_high = TRIG_VOLTAGE; 
volatile uint16_t *dac_cv_data;

bool cv_note_quant;

//enum oktave { c, cis, d, dis, e, f, fis, g, gis, a, as, b } note;


// loop variables
uint8_t  j, k, l, n;

// sprint buffer
char lcd_buffer[32];

// funktionsprotoypen //////////////////////////////////////////////////////////
static inline void update_leds();
static inline void process_stepevent(void);
static inline void set_cv_val(void);
static inline void dac_spi_transmit(uint8_t cselect,  volatile uint8_t* port, volatile uint16_t* cvdata);
//static inline void dac_spi_transmit(uint8_t cselect, uint16_t cvdata);
static inline void settimerfreq(void);
static void inline seq_hw_timer_on(void);
static void inline seq_hw_timer_off(void);
static inline void transport_handler(void);
void handle_button_press(void);
void select_param(void);
int8_t encoder_read2();
uint16_t getcv_from_noteselect(uint8_t note);
void setparavalue(void* parameter, uint16_t max, int8_t min);
uint8_t get_step_press(void);
void set_step(struct stepevent_data *_step);
void unset_step(struct stepevent_data *_step);
void toggle_mute_step(struct stepevent_data *_step);

// handle ui input//////////////////////////////////////////////////////////////
void handle_button_press(void){
	//static uint8_t __step;
	uint16_t max_val;
	uint8_t min_val;
	// verunded  mit ~ um auf loslassen zu reagieren
    ctrl = (~last_ctrl_button_scan) & ctrl_button_scan;
	selected_steps_release = (~last_seq_button_scan) & seq_button_scan;
	selected_steps_onpress = last_seq_button_scan & seq_button_scan;
    last_ctrl_button_scan = ctrl_button_scan;
	last_seq_button_scan = seq_button_scan;

	transport_handler();
	//__step = get_step_press();
	switch(machine_state){
		case normal:	
			break;
		case rec:	
			break;
			
	}
	uint8_t long_press_cnt = 50;
	if(selected_steps_onpress  || selected_steps_release){
		for(n=0; n < 16 ; n++) {
			if( ((~seq_button_scan) >>n) & 0x01 ) {
				__step = n + (at_bar * STEP_METER);

				/*long_press_cnt--;
				if( !(mask & (1<<n) ) long_press_cnt--;
				//long_press_cnt = long_press_cnt > 0 ? long_press_cnt-- : 0;
				mask |= (1<<n);*/
			}
			if( (selected_steps_release >>n & 0x01) ){
				if( long_press_cnt != 0 )	{
					if(	seq_pattern[ n + (at_bar * STEP_METER) ].note_evnt >= NOTE_ON ) 
						unset_step(&seq_pattern[n + (at_bar * STEP_METER)]); 
					else 
						set_step(&seq_pattern[n + (at_bar * STEP_METER)]); 
				}
				// conter zurücksetzen
				long_press_cnt = 50;
			} 
		}
	}

	if(ctrl & LEFT) machine_state  = rec;
	if(ctrl & RIGHT) machine_state  = normal;
	if(ctrl & ENTER)  mode ^= 1;
	if(mode == select && ~seq_button_scan ==0) {
		setparavalue(&select_state, tempo, bar);
	}
	else {
		switch(select_state) {
			case bar:	
			break;
			case step:
				parameter_sel = &__step;
				min_val = 0;
				max_val = 15;
			break;
			case note:
				parameter_sel = &(seq_pattern+__step)->note_evnt;
				min_val = 0;
				max_val = 60;
			break;
			case cv1:	
				parameter_sel = &(seq_pattern+__step)->cv1_val;
				min_val = 0;
				max_val = 4096;
			break;
			case cv2:	
				parameter_sel = &(seq_pattern+__step)->cv2_val;
				min_val = 0;
				max_val = 4096;
			break;
			case cv3:	
				parameter_sel = &(seq_pattern+__step)->cv3_val;
				min_val = 0;
				max_val = 4096;
			break;
			case nlen:	
				parameter_sel = &(seq_pattern+__step)->note_len;
				min_val = 0;
				max_val = 5;
			break;
			case timingticks:	
				parameter_sel = &(seq_pattern+__step)->tickstamp;
				min_val = -5;
				max_val = 5;
			break;
			case tempo:	
				min_val = BPM_MIN;
				max_val = BPM_MAX;
				parameter_sel = &bpm;
			break;
		}
		setparavalue(parameter_sel, max_val, min_val);
	}
}

int8_t encoder_read2( void ) {         // two  step encoders
    int8_t val;
  
    cli();
    val = enc_delta;
    enc_delta = val & 3 ;
    sei();
    return val>> 2;
}
// sequenzer core-funktionen ///////////////////////////////////////////////////////////////
//
static inline void settimerfreq(void){
	OCR1A =  (60 * F_CPU / 64) / (bpm *24) -1;
}

static inline void process_stepevent(void)	{
	if(mod6_cnt == seq_pattern[note_pos].tickstamp && seq_pattern[note_pos].note_evnt > 127 ){
		dac_cv_data = &gate_trig_high;
		//gate_trig_high = trig_voltage; //0b0000111111111111;
		dac_spi_transmit(DAC_CS_CV, &PORTB,  &(seq_pattern + note_pos)->cv1_val);
		dac_spi_transmit(DAC_CS_CV2, &PORTL, &(seq_pattern + note_pos)->cv2_val);
		dac_spi_transmit(DAC_CS_CV3, &PORTL, &(seq_pattern + note_pos)->cv3_val);
	}
	else if(mod6_cnt == 1){
		dac_cv_data = &gate_trig_low;
		//gate_trig_high = 0;
	}

	dac_spi_transmit(DAC_CS_GATE, &PORTB, dac_cv_data);
//	dac_spi_transmit(dac_cs_gate, gate_trig_high);
	// synchrones setzen der ausganglatches
	PORTB &= ~(1<<DAC_LDAC);
	PORTB |= (1<<DAC_LDAC);
}

static inline void update_leds() {
	uint8_t ledcnt;
	uint8_t _step;
    for (ledcnt=STEP_METER; ledcnt>0; ledcnt--)	{
    	// xor der leds von sequencer step und  selected steps
		_step = machine_state == rec ? (seq_pattern[ (ledcnt+ (at_bar * STEP_METER)) -1].note_evnt>>7)  : 0x00;
    	if ( (((1<<step_pos)>>ledcnt-1) ^ _step) & 0x01 ){
    		PORTA |= (1<<D_IN_595);
		}
    	else{
    		PORTA &= ~(1<<D_IN_595);
		}

    	// pf pulse clk / nächstes bit
    	PORTA |= (1<<SH_CLK_595);
    	PORTA &= ~(1<<SH_CLK_595);
    }
    // pf pulse clk / lege eingetaktete bits auf output
    PORTA |= (1<<ST_CLK_595);
    PORTA &= ~(1<<ST_CLK_595);
}

void init_seq_pattern(void) {
    int i;
    for(i=0; i<16; i++) {
	    seq_pattern[i].tickstamp = 0;
	    seq_pattern[i].note_evnt= 128;
	    seq_pattern[i].cv1_val= 1550;
	    seq_pattern[i].cv2_val= 3022;
	}
}
void set_step(struct stepevent_data* _step){
    (*_step).note_evnt = 128;
}
void unset_step(struct stepevent_data *_step) {
    (*_step).tickstamp = 0;
    (*_step).note_evnt = 0;
    (*_step).note_len = 0;
    (*_step).cv1_val = 0;
}

void toggle_mute_step(struct stepevent_data* _step){
    (*_step).note_evnt ^= 128;
}
uint8_t get_step_press(void) {
	uint8_t _step;
	if (seq_button_scan & last_seq_button_scan) {
		for(l=0; l <16; l++) {
			if (((~seq_button_scan >> l) & 0x1)) _step = l; 
		}
	}
	return _step; 
}
	

uint16_t getcv_from_note_evnt(uint8_t* note_evnt) {
	uint16_t _cv_val;
	//ausmaskieren von note_on bzw. midi noten 0..127
	_cv_val  = voltoktave[(*note_evnt & 127)]; 	
	return _cv_val;
}

void setparavalue(void *parameter, uint16_t max, int8_t min) {
	if(max > 255){
	    uint16_t _parameter = *(uint16_t*)parameter + encoder_read2();  
	    if ( _parameter <= max && _parameter >= min ) *(uint16_t*)parameter = _parameter;
	}
	else {
	    uint8_t _parameter = *(uint8_t*)parameter + encoder_read2();  
		if ( _parameter <= max && _parameter >= min ) *(uint8_t*)parameter = _parameter;
	} 
}


static inline void transport_handler(void) {
	if(ctrl & PLAY_RESTART) {
		if(!playing){
			seq_hw_timer_on();
			playing =1;
		} else {
			tick_cnt = 0;
			mod6_cnt = 0;
		}
	}
	if(ctrl & STOP_PAUSE) {
		if(playing){
			seq_hw_timer_off();
			playing = 0;
		} else {
			tick_cnt = 0;
			mod6_cnt = 0;
		}
	}
}

static void inline seq_hw_timer_on(void) {
	TIMSK1 = (1<<OCIE1A);
	TCCR1B |= (1<<CS11)|(1<<CS10);		// timer vorteiler: 64
}
static void inline seq_hw_timer_off(void) {
	TIMSK1 = 0; 
	TCCR1B &= ~((1<<CS11)|(1<<CS10));		// timer vorteiler: 64
}
// dac-rountinen ///////////////////////////////////////////////////////////////
//
static inline void dac_spi_transmit(uint8_t cSelect,  volatile uint8_t* port, volatile uint16_t* cvdata) {
//static inline void dac_spi_transmit(uint8_t cselect, uint16_t cvdata) {
	uint16_t dac_data = (1<<DAC_BUF_OUT)|(1<<DAC_XGAIN)|(1<<DAC_POWER_DOWN) | *cvdata;// 0b0111000000000000 | cvdata;
	uint8_t data[2] = {0,0};

    data[0] = ((dac_data & 0xff00)>>8); // erstes byte
    data[1] =  dac_data & 0x00ff; // zweites byte
	
	//spi_transmit(data);
    /* start transmission */
    *port &= ~(1<<cSelect);
    SPDR =  data[0];
    // wait for transmission complete 
    while(!(SPSR & (1<<SPIF))) ;
    SPDR = data[1];
    // wait for transmission complete 
    while(!(SPSR & (1<<SPIF))) ;
    *port |= (1<<cSelect);
	/* end stransmission */
}

// interuptroutinen ////////////////////////////////////////////////////////////
// sequenzer timing isr
ISR(TIMER1_COMPA_vect)	{
	note_pos = tick_cnt / 6;
	step_pos = note_pos % STEP_METER;
	at_bar = note_pos / STEP_METER;
	process_stepevent();
	tick_cnt++;
	mod6_cnt++;
	if(tick_cnt == 192) tick_cnt = 0;
	if(mod6_cnt == 6 ) mod6_cnt = 0;
	settimerfreq();
}

ISR(TIMER3_COMPA_vect) {
    seq_button_scan = 0;
    ctrl_button_scan = 0;

    // pulse /pl lade shift-eingänge in internes leseregister
    SHIFT_PORT_165 |= (1<<CLK_INHIBIT_165);
    SHIFT_PORT_165 &= ~(1<<SH_LD_CLK_165);
	SHIFT_PORT_165 |= (1<<SH_LD_CLK_165);
    SHIFT_PORT_165 &= ~(1<<CLK_INHIBIT_165);
	//
    // einlesen der registereingänge
	uint8_t j;
    for (j=0; j<24; j++)	{
    	if (j<16 && (SHIFT_PIN_165 & 1<<6)) seq_button_scan |= (1<<j);
    	else if (SHIFT_PIN_165 & 1<<6) ctrl_button_scan |= (1<<(j-16));
		
		// pulse the clock to shift 
		SHIFT_PORT_165 &= ~(1<<CLK_165); 
		SHIFT_PORT_165 |= (1<<CLK_165);
    }


	int8_t new, diff;
	new = 0;
	if( (~ctrl_button_scan) & UP ) new = 3;
    if( (~ctrl_button_scan) & DOWN ) new ^= 1;          // convert gray to binary
	diff = last - new;               // difference last - new
	if( diff & 1 ) {                 // bit 0 = value (1)
	    last = new;                    // store new as next last
	    enc_delta += (diff & 2) - 1;   // bit 1 = direction (+/-)
	}
}


// initialisierungsroutinen ////////////////////////////////////////////////////
//
void init_encoder( void ) {
    int8_t new;
  
    new = 0;
    if(  (~ctrl_button_scan) & UP ) new = 3;
    if(   (~ctrl_button_scan) & DOWN ) new ^= 1;       // convert gray to binary
    last = new;                   // power on state
    enc_delta = 0;
}

void init_spi(void) {
    // setzen der spi pins als ausgang
    DDR_SPI = (1<<DAC_SDI_MOSI)|(1<<DAC_SCK)|(1<<DAC_CS_GATE)|(1<<DAC_CS_CV)|(1<<DAC_LDAC);
	DDRL = (1<<DAC_CS_CV2) | (1<<DAC_CS_CV3 );

    PORTB |= (1<<DAC_LDAC)|(1<<DAC_CS_CV)|(1<<DAC_CS_GATE); // CS UND UND LATCHEINGANG SIND ACTIVE-LOW
	PORTL |= (1<<DAC_CS_CV2) | (1<<DAC_CS_CV3 );

    // enable spi, master, set clock rate fck/16, setze uebertragungsmodus msb
    SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
}

void init_timer(void) {
    TIMSK1 = (1<<OCIE1A);
	//CTC MODE
    TCCR1B = (1<<WGM12)|(1<<CS11)|(1<<CS10);		// TIMER VORTEILER: 64
    OCR1A = 5207; //120 BPM - 48 HZ
	
	TIMSK3 = (1<<OCIE3A);
	//TCCR3A |= (1<<WGM01);
	TCCR3B = (1<<WGM12)|(1<<CS11)|(1<<CS10);		// TIMER VORTEILER: 641<<CS01)|(1<<CS20);
	OCR3A = 90;//1300; //1250; 

}

void init_gpio(void) {
   // SETZEN DER SCHIEBEREGISTER AUSGAENGE
    DDRA |= (1<<ST_CLK_595)|(1<<SH_CLK_595)|(1<<D_IN_595);
    DDRA |= (1<<SH_LD_CLK_165)|(1<<CLK_165);
    PORTA &= -((1<<ST_CLK_595)|(1<<SH_LD_CLK_165));

	// SETZEN DER EINGAENGE
    SHIFT_DDR_165 |= (1<<SH_LD_CLK_165)|(1<<CLK_165)|(1<<CLK_INHIBIT_165);
    SHIFT_DDR_165 &= ~(1<<S_READ_165);

    SHIFT_PORT_165 |= (1<<SH_LD_CLK_165)|(1<<CLK_INHIBIT_165);
    SHIFT_PORT_165 &= -((1<<S_READ_165)|(1<<CLK_165));

}

// main ////////////////////////////////////////////////////////////////////////
int main(void)	{
	init_gpio();    // ein u. ausgänge initialisieren
    uart_init();    // serielle schnittstellen (uart /
    init_spi();     // spi initialisieren
	init_timer();	// timer initialisieren
	init_encoder();

	select_state = bar; 
	machine_state = normal;
	init_seq_pattern();
	LCDSetup(LCD_CURSOR_BLINK);//lcdsetup(lcd_cursor_none);
    //LCDWriteStringXY(1, 1, "t#:      bpm:   mode:  step:");
    LCDWriteString("Bar#:2    bpm:120 mode:1 step:23");
	
    // setzen des global interrupt flags
    sei();
    while (1)	{
		handle_button_press();
		update_leds();
	   	printf("noteselect: %d | tickstamp: %d |step: %d| seq buttons: %d | mod6_cnt: %d | pattern: %d | select_state: %d \n ",
				getcv_from_note_evnt(&(seq_pattern[note_pos]).note_evnt),
				seq_pattern[note_pos].tickstamp,
				step_pos,
				selected_steps_release,
				mod6_cnt,
				seq_pattern[step_pos].note_evnt,
				select_state);
		if(mode == select) {
			switch(select_state) {
			}
		}
		LCDWriteString(lcd_buffer);
//    LCDWriteString("Bar#:2   bpm:120 mode:1 step:23");
//    LCDWriteString("Bar#:2   bpm:120CV1:   CV2:   CV3:   ");
//		LCDWriteIntXY(14, 1, bpm,3);
//		LCDWriteIntXY(6, 2, select_state,1);
//		LCDWriteIntXY(4, 1, at_bar+1,1);
//		LCDWriteIntXY(13, 2, __step,2);
		};

    }

