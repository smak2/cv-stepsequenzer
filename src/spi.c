}static void inline spi_putb(uint8_t databyte) {
    SPDR =  databyte;
    // Wait for transmission complete 
    while(!(SPSR & (1<<SPIF))) ;
}

static void inline spi_transmit(uint8_t cSelect, uint8_t *data_arr) {
    PORTB &= ~(1<<cSelect);
    do {
      spi_putb(*data_arr);
    }while(*data_arr++);
    PORTB |= (1<<cSelect);
}
uint8_t* dac_output(uint16_t cv_data) {
	uint16_t dac_data = 0b0111000000000000 | cvData;
	uint8_t data[2];
    data[0] = ((dac_data & 0xFF00)>>8); // Erstes Byte
    data[1] =  dac_data & 0x00FF; // Zweites Byte
	
	return data;
}
