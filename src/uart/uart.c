#include <avr/io.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "uart.h"
#define F_CPU 16000000UL			// processor clock frequency
#define BAUD 115200L
#define UBRR_VALUE 16				// Baudrate bei 16MHz und double speed
//#define UBRR_VALUE 207				// Baudrate bei 16MHz und double speed

////////////////////////////////////////////////////////////////////////////////

void uart_putc(char data) {
   while(!(UCSR0A & (1<<UDRE0)));        /* Senden, wenn UDR frei ist                    */
    UDR0 = data;
}

void uart_puts(char *string){
  do {
    uart_putc(*string);
  }while(*string++);
}

// this function is called by printf as a stream handler
// Streamhandler für printf
int uart_putc_printf(char c, FILE *stream) {
    // Übersetze \n zu \r 
    if (c == '\n') uart_putc( '\r' );
    uart_putc(c);
    return 0;
}

void uart_init(void){
  UCSR0A |= (1<<U2X0);				// Double Speed
  UCSR0B |= (1<<TXEN0)|(1<<UCSZ02);		// Aktiviere Transmitter.
  UCSR0C |= (1 << UCSZ01)|(1 << UCSZ00);	// 8Bit Character size. UCSZn2

  /*
  // Baudrate einstellen. Beschreiben der Register MUSS high VOR low sein!
  UBRR0H = UBRR_VALUE >> 8;			// Verstehe ich noch nicht
  UBRR0L = UBRR_VALUE & 0xFF;			// so richtig.
  */
  
  // Gleiche wie oben aber Compiler regelt die Reihenfolge des Registerzugriffs
  UBRR0 = UBRR_VALUE;
 
  // Verknüpfe Stdout mit Streamhandler für printf
  static FILE mystdout = FDEV_SETUP_STREAM(uart_putc_printf, NULL, _FDEV_SETUP_WRITE);
  stdout = &mystdout; 
}

