const char c0[] PROGMEM = "C0";
const char c1[] PROGMEM = "C1";
const char c2[] PROGMEM = "C2";
const char c3[] PROGMEM = "C3";
const char c4[] PROGMEM = "C4";
const char c5[] PROGMEM = "C5";
const char c6[] PROGMEM = "C6";
const char c7[] PROGMEM = "C7";
const char c8[] PROGMEM = "C8";
const char c9[] PROGMEM = "C9";

const char cis0[] PROGMEM = "Cis0";
const char cis1[] PROGMEM = "Cis1";
const char cis2[] PROGMEM = "Cis2";
const char cis3[] PROGMEM = "Cis3";
const char cis4[] PROGMEM = "Cis4";
const char cis5[] PROGMEM = "Cis5";
const char cis6[] PROGMEM = "Cis6";
const char cis7[] PROGMEM = "Cis7";
const char cis8[] PROGMEM = "Cis8";
const char cis9[] PROGMEM = "Cis9";

const char d0[] PROGMEM = "D0";
const char d1[] PROGMEM = "D1";
const char d2[] PROGMEM = "D2";
const char d3[] PROGMEM = "D3";
const char d4[] PROGMEM = "D4";
const char d5[] PROGMEM = "D5";
const char d6[] PROGMEM = "D6";
const char d7[] PROGMEM = "D7";
const char d8[] PROGMEM = "D8";
const char d9[] PROGMEM = "D9";

const char dis0[] PROGMEM = "Dis0";
const char dis1[] PROGMEM = "Dis1";
const char dis2[] PROGMEM = "Dis2";
const char dis3[] PROGMEM = "Dis3";
const char dis4[] PROGMEM = "Dis4";
const char dis5[] PROGMEM = "Dis5";
const char dis6[] PROGMEM = "Dis6";
const char dis7[] PROGMEM = "Dis7";
const char dis8[] PROGMEM = "Dis8";
const char dis9[] PROGMEM = "Dis9";

const char* const note_strings[] PROGMEM = {c0,c1,c2,c3,c4,c5,c6,c7,c8,c9};

// 1V / 12 Note = 83mV pro Note
const uint16_t voltoktave[60] PROGMEM = {68, 136, 205, 273, 341, 409, 477, 545, 614, 682, 750, 818, 886, 955, 1023, 1091, 1159, 1227, 1295, 1364, 1432, 1500, 1568, 1636, 1705, 1773, 1841, 1909, 1977, 2046, 2114, 2182, 2250, 2318, 2386, 2455, 2523, 2591, 2659, 2727, 2796, 2864, 2932, 3000, 3068, 3136, 3205, 3273, 3341, 3409, 3477, 3546, 3614, 3682, 3750, 3818, 3886, 3955, 4023, 4091};
