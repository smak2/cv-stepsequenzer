#!/usr/bin/python3

import sys

# Global
dac_res=4096
dac_vref = 10 

oktaven = dac_vref
noten_pro_okt = 12 # C..C'
total_notes = noten_pro_okt * oktaven
noten=('C','C#','D','D#','E','F','F#','G','G#','A','A#','H')

volt_pro_notenschritt = 1/noten_pro_okt # 0,0833333333333333333
dac_volt_quantout = dac_vref/dac_res # 0,00244140625
dac_value_stepquant = volt_pro_notenschritt/dac_volt_quantout # DAC Wert 34,13333333 entspricht 0,08333333333333333 Volt

def gen_note_cv_table():
    note = None
    notencnt=0
    dac_val_note_table = {}
    for i in range(0,total_notes):
        if notencnt == noten_pro_okt:
            notencnt = 0
        note = noten[notencnt] + str(int(i/noten_pro_okt))
        notencnt += 1
        dac_val = dac_val + dac_value_stepquant if i > 0 else 0
        dac_val_note_table[note] = { 'dac':round(dac_val), 'volt':round(volt_pro_notenschritt * i, 3) } 

    return dac_val_note_table

def formating_to_c_arr():
    return  [ note['dac'] for note in gen_note_cv_table().values() ]
    
if __name__ == '__main__':
    if sys.argv[1] == 'carry':
        print(formating_to_c_arr())
    elif sys.argv[1] == 'full':
        print(gen_note_cv_table())


