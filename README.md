# CV Step-Sequenzer
#### Allgemeines

Dieses Repository wurde im Rahmen meiner Projektarbeit an der Abschlussprojekt 
im 2. Jahrgang zum [staatl. gepr. Techniker für Elektrotechnik](https://technikerschule-muenchen.de/index.php/fachrichtungen/elektrotechnik) erstellt. Die Idee
ist es, ein Versionsionierungssystem als Grundlage für die Projektdokumentation 
zu verwenden. Die Wahl fiel auf Gitlab.

## Projektzielbeschreibung

Es soll ein CV-Lauflichtsequenzer (CV: Control-Voltage) entwickelt bzw ein Prototyp
im [Eurorackformat](https://en.wikipedia.org/wiki/Eurorack) entworfen werden, welcher
für die Ansteuerung von analogen Synthesizern mittels Steuerspannung arbeiten soll.
Ein Step-Sequenzer, im musikalischen Sinne,ist ein analoges oder elektronisches Gerät, 
welches die Möglichkeit zur Verfügungstellt, rythmische und klangliche Muster einzuprogrammieren
bzw. Aufzunehmen u. Wiederzugeben. Auf diese Art und Weise Musik zu kreieren ist heute ein
selbstverständlich und bietet viele verschiedenen Funktionen welche meist über
den digitalen MIDI-Standart und/oder über analoge Steuerspannung realisiert sind.

Die Steuerspannung im modularen Eurorack Format beträgt +/- 12 Volt und folgt der 
der Einheit Volt/Oktave, wobei eine Oktave 12 Musiknoten von C,D,E,..C darstellt.
Natürlich sind auch alle Zwischenspannung zulässig bzw. insbesondere erwünscht.
Durch Kontrollspannungen lassen sich alle denkbaren Parameter im im Klangspektrum
frei modulieren.

Grundsätzlich Funktionen welche für den Sequenzer implementiert werden sollen.
*  Vier CV Kanäle zum Ausgeben der Steuer und Gate Spannung.
*  16 Step Lauflicht zur Darstellung der Noten und des aktiven Schritts/Steps.
*  Ausgeben einer GATE Spannung (5V)
*  Eingabe von Noten
*  Ändern der CV-Spannungen pro Schritt/Step.
*  Taster und Drehgeber zur Steuerung und Änderung der Anzeige
*  LCD zur Anzeige von Information




